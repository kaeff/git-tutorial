<h1 class="fullscreen">Why you want to use Git</h1>

# Why you want to use Git

... as your VCS of choice

1. It's distributed
2. Branching is cheap
3. It's fast, lightweight and easy to use

# It's distributed

### Git is a ___D___istributed _V_ersion _C_ontrol _S_ystem

* Not a single server but a network of nodes
* Repository can be initialised spontaneously and locally
* -> No need to set up a server / repository in advance - Just push it somewhere, sometime

# It's distributed

### Think beyond

* Mirroring
* git-svn

# Branching is cheap

* Automatic merges, except that they really work this time
* Merge tracking: Git knows which commits belong to a branch an what not (aka: What you _thought_ SVN could do, but couldn't)

# Branching is cheap

### Example: Updating a forked (copied and altered) open source product to the latest version

* Projects diverged for more than 1 year
* Merge ~3000 commits from official repository
* ~100 local commits
* -> __10 merge conflicts__ resolved in 10 minutes

# It's fast, lightweight and easy to use

### Three simple recipies

1. Trees
2. Pointers
3. Blobs

# It's fast, lightweight and easy to use
### Trees

Repository is a graph of commits

# It's fast, lightweight and easy to use
### Pointers

* A commit is identified by a GUID (SHA)
* A commit references to it's parents (1, or 2 for a merge commit)
* A commit references the files affected

Some special pointers (refs)

* `<branch>`: latest commit in (local) branch
* `<remote>/<branch>`: latest commit in branch (at the remote)
* `HEAD`: latest commit in the repository

# It's fast, lightweight and easy to use
### Blobs

* All file contents are stored in blobs
* Patches + full copies once in a while
* Under the hood, it's all shell and C

# It's fast, lightweight and easy to use
### Example: Setting up a working copy

* Big project, running vor >2 years, ~10 developers, ~50 branches
* Downloading project archive: __80 MB__, time: Download speed + ~20
  seconds
* Includes _every branch and tag_!

# Differences to SVN

### Commits rather than revisions

* globally identifyable
* not linear dependant

### Remotes

* Not a single server but a network of nodes
* In practise: 'Server' is a central hosted node referred to as 'origin'

### Basically all the advantages mentioned before

<h1 class="fullscreen">Basic usage</h1>

# The command line is your friend

### There are GUIs out there (Google is your friend)

* Issue: Rarely awesome for PCs (i.e. Linux and Windows)
* Disclamer: Not as great as Tortoise (sic!)

### The command line works like a charm

* Auto completion
* Fine-grain control over what's happening
* Really awesome UX _(yes, for a command line tool!)_

### Strong advice: C'mon, give it a try

# `init` and `clone` a repository



# Making commits with `add` and `commit`

* Everything is offline first.
* Yet another layer: The staging area
  * `add` stuff to mark it as _to be committed_
  * Modified files in staging need to be re-added
* The commit message matters (style)
* Amending

# Publishing it with `push` and `pull`

### Usage

* `push` uploads and applies all _ahead_ commits
* `pull` downloads and applies all comimts you are _behind_
* Add `<remote> <branch>` to be precise

### Did you know?

* Require remote connection ( _actually, these are the only ones that do_ )
* Show status: `git remote show origin`
* Download-only: `fetch` now, `rebase` later

# Yet another layer: Fooling around with `stash`

### Imagine stash as your __extended clipboard__

* `git stash` to stash
* `git stash pop` to unstash
* More: `git stash list` & `git stash delete`

### But what for?

* Quickly move something aside (e.g. for urgend bugfix / code review)
* Move WIP across branches
* Split big WIP into several commits 

# Painless separation of concerns with `branch` and `merge`

* `git -b`
* Tracking branches with `git branch --track`
* `git merge <branch>`
* Resolving conflicts

# Correcting mistakes with `reset` and `revert`

### reset vs. revert

* `git reset` points your working copy to a commit 
  * `--soft`: ...without touching the files
  * `--hard`: ...and resets the files
* `git revert` reverse-applies a commit

  * Records a new commit that does the exact opposite of another one

### Something went wrong? No problem

* Git blobs are read+append-only
* `git reflog` allows time travling


<h1 class="fullscreen">Nice to know</h1>

# Wait! There's more

* `mv` renames and moves files (which is tracked)
* `rm` deletes version-controled files
* `bisect`, `grep` and `log` let you browse the history
* `rebase` let's you nicen (and fuck) up the history
* `cleanup` removes non versioned-files (not ignored ones)

# Github = Hosting + Gui + Community

### Viewing content

* Basically a nicer-looking git-web
* View all kinds of stuff
  * Branches
  * Files
  * Diffs

# Github = Hosting + Gui + Community

### Collaborate

* Code Reviews ( _Diff-/Commit-/Line-level commenting_ )
* Wiki
* Issues

# Github = Hosting + Gui + Community

### Social Network

* Forks ( _Cross-repository branching_ )
* Pull Requests ( _Please merge my changes into yours_ )
* Gists ( _Version-controlled (ediable, forkable) pastebin for snippets_ )
* Pages ( _Web hosting at GitHub_ )

# Github = Hosting + Gui + Community

### Open Source Spirit

# Configuring git with `~/.gitconfig`

* User name/email
* merge tool
* Global ignores (in `~/.gitignore`)
* Aliases, e.g.

>\[alias\]
>  c = commit
>  s = status
>  a = add
>  co = checkout
>  sh = stash
>  d = difftool
>  rs = reset
>  rv = revert
>  b = branch

<h1 class="fullscreen">Workflow: Feature-Branch pattern</h1>
_(As suggested for our project)_

# Workflow: Feature-Branch pattern


### Idea: Have a single branch for each feature story in your bug tracker

# Workflow: Feature-Branch pattern

### Why?

* Why not? Atomarity of changes is great and branching is cheap
* Speed: Instantly start coding
* Embraces commiting early and often: You can hardly break anything
* Visibility: Which feature brought which commit
* Easy status monitoring
* Easy code reviews
* Let's remember SVN: Stable master, Early commits - You cannot have
  both

### -> Totally lean

# Workflow: Feature-Branch pattern

### Assumptions

* Ticket tracker outputs some sort of project-wide story ID (Can be
  assumed)
* Deployment pipeline: Local -> Staging -> Prodcution (Not necessary)
* Workflow for single version product (could be scaled to multi-version
  product)

# Workflow: Feature-Branch pattern

### Branches involved

* `master`: The always-building state of the next version to be released
* `development`: Staging area
* `<ticketnumber>-<short-description>` A branch for every story

# Workflow: Feature-Branch pattern

### Workflow

1. Fork feature branch from master
2. Work on story
3. Unit test locally
3. Integration and acceptance test in staging
4. Story is marked for code review
5. Story is marked as reviewed
6. Deploy

# Workflow: Feature-Branch pattern

### 1) Fork feature branch from master

    git pull origin master
    git -b <feature-branch-name>

Remember: `<feature-branch-name>` = `<ticketnumber>-<short-description>`

# Workflow: Feature-Branch pattern

### 2) Work on story

* Commit early, commit often
* Push as soon as it is coherent

# Workflow: Feature-Branch pattern

### 3) Unit test locally

Once story finished:

* Test
* `git pull origin <feature-branch-name>`
* Test again

# Workflow: Feature-Branch pattern

### 4) Integration and acceptance test in staging

    git development
    git pull origin development
    git fetch origin
    git merge <feature-branch-name>

Deploy to staging

# Workflow: Feature-Branch pattern

### 5) Story is marked for code review

A second developer reviews the code on his machine for QA

    git fetch origin
    git <feature-branch-name>
    git pull origin <feature-branch-name>

* Reviewer may only commit small changes, such format, whitespace issues etc.
* As long as review result is REJECT, repeat steps 2-4
* As soon as review result is APPROVE, proceed to step 5

# Workflow: Feature-Branch pattern

### 6) Story is marked as reviewed

The maintainer merges the feature branch into master since the changes
are QAed

    git master
    git pull origin master
    git merge <feature-branch-name>
    git push origin master

# Workflow: Feature-Branch pattern

### 7) Deploy


<h1 class="fullscreen">If you didn't remember anything...</h1>

# Summary

* It's distributed
* Branching is cheap
* It's fast, lightweight and easy to use
* Server = Remote
* clone -> -> pull -> add -> commit -> push
* Know your weapons: checkout, reset, revert, merge

<h1 class="fullscreen">Happy coding</h1>
